<?php
	
	Router::resourceMap( array(
        array( 'action' => 'index', 'method' => 'GET', 'id' => false ),
        array( 'action' => 'view', 'method' => 'GET', 'id' => true ),
        array( 'action' => 'add', 'method' => 'POST', 'id' => false),
        array( 'action' => 'edit', 'method' => 'POST', 'id' => true ),
        array( 'action' => 'delete', 'method' => 'DELETE', 'id' => true ),
    ) );
    
    
    Router::mapResources( 
	    array(
	    	'Api.Assunto',
			'Api.Atapreco',
			'Api.Atendente',
			'Api.AtendentesEndereco',
			'Api.AtendentesFone',
			'Api.Atividade',
			'Api.Cargo',
			'Api.Chamada',
			'Api.ChamadasProcedimento',
			'Api.Cidade',
			'Api.Contato',
			'Api.Convenio',
			'Api.Contatoemail',
			'Api.Contatoendereco',
			'Api.Contatofone',
			'Api.Contatofornecedor',
			'Api.Contatoinstituicao',
			'Api.Distribuidor',
			'Api.Edital',
			'Api.Etapa',
			'Api.Fornecedor',
			'Api.Instituicao',
			'Api.Item',
			'Api.Orgao',
			'Api.Prioridade',
			'Api.Procedimento',
			'Api.Processo',
			'Api.Projeto',
			'Api.Sexo',
			'Api.Situacaocontato',
			'Api.Status',
			
			'Api.Tipochamada',
			'Api.Tipoconvenio',
			'Api.Tipodocumento',
			'Api.Tipoemail',
			'Api.Tipoendereco',
			'Api.Tipoinstituicao',
			'Api.Tipopagamento',
			'Api.Tipotelefone'
		)
    );
    


    Router::parseExtensions();
   	