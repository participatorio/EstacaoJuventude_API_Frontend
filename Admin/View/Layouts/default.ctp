<!DOCTYPE html>
<html ng-app="estacaoApp" ng-controller="mainController">
	<header >
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">  

		<title>Estação Juventude</title>
		
		<!-- CSS -->
		<!-- Angular -->
		<link rel="stylesheet" href="/admin/bower_components/angular/angular-csp.css">
		<!-- Twitter Bootstrap -->
		<link rel="stylesheet" href="/admin/bower_components/bootstrap/dist/css/bootstrap.css">
		<!-- Theme -->
		<link rel="stylesheet" ng-href="/admin/bower_components/bootswatch/{{ cssTheme }}/bootstrap.css">
		<!-- Font Awesome -->
		<link rel="stylesheet" href="/admin/bower_components/font-awesome/css/font-awesome.min.css">
		<!-- App CSS -->
		<link rel="stylesheet" href="/admin/css/style.css">
		<!-- Scripts -->
		<script src="//maps.googleapis.com/maps/api/js"></script>
		<script src="/admin/bower_components/jquery/dist/jquery.min.js"></script>
		<!-- Twitter Bootstrap -->
		<script src="/admin/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
		<!-- Angular JS -->
		<script src="/admin/bower_components/angular/angular.min.js"></script>
		<!-- Angular Modules -->
		<!-- TextAngular -->
		<link rel="stylesheet" href="/admin/bower_components/textAngular/src/textAngular.css">
		<script src="/admin/bower_components/textAngular/dist/textAngular-rangy.min.js"></script>
		<script src="/admin/bower_components/textAngular/dist/textAngular-sanitize.min.js"></script>
		<script src="/admin/bower_components/textAngular/dist/textAngular.min.js"></script>
		<!-- Angular UI Bootstrap -->
		<script src="/admin/bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js"></script>
		<!-- ng-Maps -->
		<script src="/admin/bower_components/ngmap/build/scripts/ng-map.min.js"></script>
		<!-- Route -->
		<script src="/admin/bower_components/angular-route/angular-route.min.js"></script>
		<!-- Classy -->
		<script src="/admin/bower_components/angular-classy/angular-classy.min.js"></script>
		<!-- Message Center -->
		<script src="/admin/bower_components/message-center/message-center.js"></script>
		<!-- Angular App -->
		<script src="/admin/js/Angular/app.js"></script>
		<!-- Angular ngRoutes -->
		<script src="/admin/js/Angular/routes.js"></script>
		<!-- Angular Services -->
		<script src="/admin/js/services/data.js"></script>
		<!-- Angular Controllers -->
		<!-- main files -->
		<script src="/admin/js/Angular/mainController.js"></script>
		<script src="/admin/js/Angular/dashboard/controllers.js"></script>
		<!-- security files -->
		<script src="/admin/js/Angular/menu/controllers.js"></script>
		<script src="/admin/js/Angular/usuario/controllers.js"></script>
		<script src="/admin/js/Angular/grupo/controllers.js"></script>
		<script src="/admin/js/Angular/permissao/controllers.js"></script>
		<!-- modules files -->
		<script src="/admin/js/Angular/tematica/controllers.js"></script>
		<script src="/admin/js/Angular/ocorrencia/controllers.js"></script>
		<script src="/admin/js/Angular/programa/controllers.js"></script>
		<script src="/admin/js/Angular/mensagem/controllers.js"></script>
		<!-- table files -->
		<script src="/admin/js/Angular/estado/controllers.js"></script>
		<script src="/admin/js/Angular/municipio/controllers.js"></script>

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 10]>
			<script src="/admin/bower_components/html5shiv/dist/html5shiv.min.js"></script>
			<script src="/admin/bower_components/respond/dest/respond.min.js"></script>
		<![endif]--> 
	</header>
	<body>
		<?php echo $this->fetch('content'); ?>
		<?php echo $this->element('sql_dump'); ?>
	</body>
</html>

