<div class="row">
	<div class="col-md-4">&nbsp;</div>
	<div class="col-md-4">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h3 class="panel-title">Login</h3>
			</div>
			<div class="panel-body">
				
				<?php echo $this->Form->create('Usuario'); ?>
				
				<?php echo $this->Form->input('login',array('label'=>'Usuário','class'=>'form-control')); ?>
				
				<?php echo $this->Form->input('senha', array('class'=>'form-control','type'=>'password','value'=>'')); ?>
				<hr>
				<?php echo $this->Form->submit('Login',array('class'=>'btn btn-primary')); ?>
				<?php if(isset($error)) {?>
				<br>
				<div class="alert alert-danger"><?php echo $error; ?></div>
				<?php } ?>
				<?php echo $this->Form->end(); ?>
				
			</div>
		</div>	
	</div>
	<div class="col-md-4">&nbsp;</div>
</div>