<?php
	class UsuariosController extends AdminAppController {
		
		public function login() {
			
			if ($this->request->is('post')) {
				
				if ($this->Auth->login($this->request->data)) {
					return $this->redirect($this->Auth->redirectUrl());
				} else {
					$this->set('error', 'Usuário ou Senha incorretos!');
				}
				
			}
			
		}
		
		public function logout() {
			$this->redirect($this->Auth->logout());
			
		}
		
	}