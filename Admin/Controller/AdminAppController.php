<?php
	class AdminAppController extends AppController {
		
		public $components = array(
			'Auth' => array(
				'loginAction' => array(
					'controller' => 'usuarios',
					'action' => 'login',
					'plugin' => 'admin'
				),
				'authenticate' => array(
					'Form' => array(
						'fields' => array(
							'username' => 'login',
							'password' => 'senha'
						)
					)
				)
			)
		);
		
		//$this->Auth->authError 

	}