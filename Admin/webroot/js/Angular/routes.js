estacaoApp.config(
	['$routeProvider',
	function($routeProvider) {
	$routeProvider
	.when('/', {
		templateUrl: '/admin/js/Angular/dashboard/screen.html',
		controller: 'dashboardCtrl'
	})
	// Usuarios
	.when('/usuario', {
		templateUrl: '/admin/js/Angular/usuario/screen.html',
		controller: 'usuarioCtrl'
	})
	// Ocorrencias
	.when('/ocorrencia', {
		templateUrl: '/admin/js/Angular/ocorrencia/listing.html',
		controller: 'ocorrenciaCtrl'
	})
	.when('/ocorrencia/add', {
		templateUrl: '/admin/js/Angular/ocorrencia/form.html',
		controller: 'ocorrenciaFormCtrl'
	})
	.when('/ocorrencia/edit/:id', {
		templateUrl: '/admin/js/Angular/ocorrencia/form.html',
		controller: 'ocorrenciaFormCtrl'
	})
	// Menus
	.when('/menu', {
		templateUrl: '/admin/js/Angular/menu/screen.html',
		controller: 'menuCtrl'
	})
	// Estados
	.when('/estado', {
		templateUrl: '/admin/js/Angular/estado/listing.html',
		controller: 'estadoCtrl'
	})
	.when('/estado/add', {
		templateUrl: '/admin/js/Angular/estado/form.html',
		controller: 'estadoFormCtrl'
	})
	.when('/estado/edit/:id', {
		templateUrl: '/admin/js/Angular/estado/form.html',
		controller: 'estadoFormCtrl'
	})
	// Municipios
	.when('/municipio', {
		templateUrl: '/admin/js/Angular/municipio/listing.html',
		controller: 'municipioCtrl'
	})
	.when('/municipio/add', {
		templateUrl: '/admin/js/Angular/municipio/form.html',
		controller: 'municipioFormCtrl'
	})
	.when('/municipio/edit/:id', {
		templateUrl: '/admin/js/Angular/municipio/form.html',
		controller: 'municipioFormCtrl'
	})
	// Tematicas
	.when('/tematica', {
		templateUrl: '/admin/js/Angular/tematica/listing.html',
		controller: 'tematicaCtrl'
	})
	.when('/tematica/add', {
		templateUrl: '/admin/js/Angular/tematica/form.html',
		controller: 'tematicaFormCtrl'
	})
	.when('/tematica/edit/:id', {
		templateUrl: '/admin/js/Angular/tematica/form.html',
		controller: 'tematicaFormCtrl'
	})
	// Grupos
	.when('/grupo', {
		templateUrl: '/admin/js/Angular/grupo/listing.html',
		controller: 'grupoCtrl'
	})
	// Permissoes
	.when('/permissao', {
		templateUrl: '/admin/js/Angular/permissao/screen.html',
		controller: 'permissaoCtrl'
	})
	// Programas
	.when('/programa', {
		templateUrl: '/admin/js/Angular/programa/screen.html',
		controller: 'programaCtrl'
	})
	// Mensagens
	.when('/mensagem', {
		templateUrl: '/admin/js/Angular/mensagem/inbox.html',
		controller: 'mensagemCtrl'
	})
	// Erros
	.otherwise({
		templateUrl: '/admin/js/Angular/errors/404.html'
	});
}]);