estacaoApp.cC({
	name: 'mainController',
	inject: ['$scope'],
	init: function() {
		this.$.cssTheme = 'lumen';
	},
	methods: {
		changeTheme: function(theme) {
			this.$.cssTheme = theme;
		},
		help: function() {
			console.log('Ajuda');
		}
	}
});