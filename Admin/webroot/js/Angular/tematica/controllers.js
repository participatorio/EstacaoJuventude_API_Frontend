estacaoApp.cC({
	name: 'tematicaCtrl',
	inject: ['$scope','$http','Data','$routeParams'],
	init: function() {
		this.load(1);
		this.$.currentModel = 'Tematica';
		this.Data.init();
	},
	methods: {
		load: function(page) {
			this.$http
			.get('/api/tematica/?populate=Pai,Filhos&order=Tematica.nome.asc&where=Tematica.parent_id.null')
			.success(function(data) {
				this.$.listing = data.data;
			}.bind(this));
		},
		del: function(item) {
			if (window.confirm('Tem Certeza?')) {
				this.$http
				.delete('/api/tematica/index/'+item.Estado.id)
				.success(function() {
					messageCenterService.add('success', 'Temática excluída com sucesso!', { timeout: 3000 })
					this.load(this.$.paginator.page);
				}.bind(this))
				.error(function() {
					messageCenterService.add('error', 'Erro ao excluir!', { timeout: 3000 })					
				}.bind(this));
			}
		},
		go: function(where, id) {
			
			switch (where) {
				case 'add':
					location.href = '#/tematica/add';
					break;
				case 'edit':
					location.href = '#/tematica/edit/'+id;
					break;
			}
		}
	}

});

estacaoApp.cC({
	name: 'tematicaFormCtrl',
	inject: ['$scope','$http','Data','$routeParams'],
	init: function() {
		this.$.currentModel = 'Tematica';
		if (this.$routeParams.id) {
			this.edit();
		} else {
			this.add();
		}	
	},
	methods: {
		_related: function() {
			// Tematica pai
			this.$http
			.get('/api/tematica?order=Tematica.nome.asc&limit=100&where=Tematica.parent_id.null')
			.success(function(data){
				this.$.pais = data.data;
			}.bind(this));
			
		},
		add: function(item) {
			this._related();
			this.$.form = {
			};
			this.$.screen = 'form';
	
		},
		edit: function() {
			this._related();
			this.$http
			.get('/api/tematica/index/'+this.$routeParams.id)
			.success(function(data){
				this.$.form = data;
			}.bind(this));
		},
		save: function() {
			this.$http
			.post(
				'/api/tematica/index',
				this.$.form
			)
			.success(function(data) {
				this.Data.flashMsgs.push({
					type:'success', 
					msg: 'Salvo com sucesso!', 
					options: { timeout: 3000 }
				});
				location.href = '#/tematica';
			}.bind(this))
			.error(function() {
				this.messageCenterService.add('danger', 'Erro ao salvar!', { timeout: 3000 });
				location.href = '#/tematica';	
			}.bind(this));
		},
		cancel: function() {
			location.href = '#/tematica';
		}
	}
});

