estacaoApp.cC({
	name: 'programaCtrl',
	inject: ['$scope','$http','messageCenterService'],
	init: function() {
		this.$.screen = 'listing';
		this.$.listing = [];
		this.load(1);
	},
	load: function(page) {
		this.$http
		.get('/api/programa/index/page:'+page+'?populate=OcorrenciaMunicipal')
		.success(function(data) {
			this.$.listing = data.data;
			this.$.paginator = data.paginator;
		}.bind(this));
	},
	edit: function(item) {
		this.$http
		.get('/api/grupo?order=Grupo.nome.asc&limit=100')
		.success(function(data) {
			this.$.grupos = data.data;
			this.$.form = item;
			this.$.screen = 'form';
		}.bind(this));
	},
	add: function(item) {
		this.$http
		.get('/api/grupo?order=Grupo.nome.asc&limit=100')
		.success(function(data) {
			this.$.grupos = data.data;
			this.$.form = {
				Usuario: {}
			};
			this.$.screen = 'form';
		}.bind(this));
	},
	save: function() {
		this.$http
		.post(
			'/api/programa/index',
			this.$.form
		)
		.success(function(data) {
			this.messageCenterService.add('success', 'Usuário salvo!', { timeout: 3000 });
			this.load(this.$.paginator.Usuario.page);
			this.$.screen = 'listing';	
		}.bind(this))
		.error(function() {
			this.messageCenterService.add('danger', 'Erro ao salvar!', { timeout: 3000 });
			this.$.screen = 'listing';	
		}.bind(this));
	},
	cancel: function() {
		this.$.form = [];
		this.$.screen = 'listing';		
	},
	del: function(item) {
		this.$http
		.delete('/api/programa/index/'+item.Usuario.id)
		.success(function() {
			this.load(this.$.paginator.Usuario.page);
			this.$.screen = 'listing';
		}.bind(this));
	}
});
