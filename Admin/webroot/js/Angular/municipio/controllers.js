estacaoApp.cC({
	name: 'municipioCtrl',
	inject: ['$scope','$http','Data','$routeParams'],
	init: function() {
		this.load(1);
		this.Data.init();
	},
	methods: {
		load: function(page) {
			this.$http
			.get('/api/municipio/index/page:'+page+'?populate=Estado&order=Municipio.nome.asc')
			.success(function(data) {
				this.$.listing = data.data;
				this.$.paginator = data.paginator;
			}.bind(this));
		},
		del: function(item) {
			if (window.confirm('Tem Certeza?')) {
				this.$http
				.delete('/api/municipio/index/'+item.Estado.id)
				.success(function() {
					messageCenterService.add('success', 'Município excluído com sucesso!', { timeout: 3000 })
					this.load(this.$.paginator.page);
				}.bind(this))
				.error(function() {
					messageCenterService.add('error', 'Erro ao excluir!', { timeout: 3000 })					
				}.bind(this));
			}
		},
		go: function(where, item) {
			
			switch (where) {
				case 'add':
					location.href = '#/municipio/add';
					break;
				case 'edit':
					location.href = '#/municipio/edit/'+item.Municipio.id;
					break;
			}
		}
	}

});

estacaoApp.cC({
	name: 'municipioFormCtrl',
	inject: ['$scope','$http','Data','$routeParams'],
	init: function() {
		if (this.$routeParams.id) {
			this.edit();
		} else {
			this.add();
		}	
	},
	methods: {
		_related: function() {
			// Estados
			this.$http
			.get('/api/estado/index?order=Estado.nome.asc&limit=50')
			.success(function(data) {
				this.$.estados = data.data;
			}.bind(this));
		},
		add: function(item) {
			this._related();
			this.$.form = {
			};
			this.$.screen = 'form';
	
		},
		edit: function() {
			this._related();
			this.$http
			.get('/api/municipio/index/'+this.$routeParams.id)
			.success(function(data){
				this.$.form = data;
			}.bind(this));
		},
		save: function() {
			this.$http
			.post(
				'/api/municipio/index',
				this.$.form
			)
			.success(function(data) {
				this.Data.flashMsgs.push({
					type:'success', 
					msg: 'Salvo com sucesso!', 
					options: { timeout: 3000 }
				});
				location.href = '#/municipio';
			}.bind(this))
			.error(function() {
				this.messageCenterService.add('danger', 'Erro ao salvar!', { timeout: 3000 });
				location.href = '#/municipio';	
			}.bind(this));
		},
		cancel: function() {
			location.href = '#/municipio';
		}
	}
});

