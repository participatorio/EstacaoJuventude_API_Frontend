estacaoApp.cC({
	name: 'ocorrenciaCtrl',
	inject: ['$scope','$http','Data'],
	init: function() {
		this.$.currentModel = 'OcorrenciaMunicipal';
		this.$.formName = 'formOcorrencia';
		this.$.filters = '';
		this.$.statusSelected = 0;
		this.$.situacaoSelected = 0;
		this.load(1);
	},
	methods: {
		load: function(page) {
			this.$.listing = [];
			this.$.empty = false;
			var populate = 'Municipio,Programa,Status,Situacao';
			var order = 'OcorrenciaMunicipal.inicio_inscricoes.desc';
			this.$http
			.get('/api/ocorrencia_municipal/index/page:'+page+'?populate='+populate+this.$.filters+'&order='+order)
			.success(function(data) {
				if (data.data.length == 0) {
					this.$.empty = true;
				}
				this.$.listing = data.data;
				this.$.paginator = data.paginator;
			}.bind(this));
		},
		del: function(id) {
			this.$http
			.delete('/api/ocorrencia_municipal/index/'+id)
			.success(function() {
				this.load(1);
			}.bind(this));
		},
		filter: function(codigo) {
			switch (codigo) {
				case 's0':
					this.$.statusSelected = 0;
					this.$.situacaoSelected = 0;
					$('label.active').removeClass('active');
					break;
				case 's1':
					this.$.statusSelected = 1;
					break;
				case 's2':
					this.$.statusSelected = 2;
					break;
				case 'i1':
					this.$.situacaoSelected = 1;
					break;
				case 'i2':
					this.$.situacaoSelected = 2;
					break;
				case 'i3':
					this.$.situacaoSelected = 3;
					break;			
			}
			
			if (this.$.statusSelected == 0 & this.$.situacaoSelected == 0) {
				this.$.filters = '';				
			} else {
				this.$.filters = '&where=';
			}
			
			if (this.$.statusSelected != 0) {
				this.$.filters += 'OcorrenciaMunicipal.status_id.eq.'+this.$.statusSelected;
			}
			if (this.$.situacaoSelected != 0 && this.$.situacaoSelected != 3) {
				if (this.$.statusSelected != 0) this.$.filters += ',';
				this.$.filters += 'OcorrenciaMunicipal.situacao_id.eq.'+this.$.situacaoSelected;
			}
			if (this.$.situacaoSelected == 3) {
				data = new Date();
				data = data.getFullYear()+'-'+(data.getMonth()+1)+'-'+data.getDate();
				if (this.$.statusSelected != 0) this.$.filters += ',';
				this.$.filters += 'OcorrenciaMunicipal.situacao_id.eq.1,OcorrenciaMunicipal.fim_inscricoes.lt.'+data;
			}
			this.load(1);
		},
		go: function(where, id) {
			
			switch (where) {
				case 'add':
					location.href = '#/ocorrencia/add';
					break;
				case 'edit':
					location.href = '#/ocorrencia/edit/'+id;
					break;
			}
		}
	}

});
estacaoApp.cC({
	name: 'ocorrenciaFormCtrl',
	inject: ['$scope','$http','Data','$routeParams'],
	init: function() {
		if (this.$routeParams.id) {
			this.edit();
		} else {
			this.add();
		}
	},
	methods: {
		related: function() {
			this.$http
			.get('/api/status?order=Status.nome.asc')
			.success(function(data){
				this.$.status = data.data;
			}.bind(this));
			this.$http
			.get('/api/situacao?order=Situacao.nome.asc')
			.success(function(data){
				this.$.situacoes = data.data;
			}.bind(this));
		},
		edit: function() {
			this.related();
			this.$http
			.get('/api/ocorrencia_municipal/index/'+this.$routeParams.id+'?populate=Municipio,Programa')
			.success(function(data) {
				// Converter string para data nos campos necessarios
				data['OcorrenciaMunicipal']['inicio_inscricoes'] = new Date(data['OcorrenciaMunicipal']['inicio_inscricoes']+' GMT:-03:00');
				data['OcorrenciaMunicipal']['fim_inscricoes'] = new Date(data['OcorrenciaMunicipal']['fim_inscricoes']+' GMT:-03:00');
				this.$.form = data;
				// Monta combo com o municipio atual
				this.$http
				.get('/api/municipio?order=Municipio.nome.asc&where=Municipio.id.eq.'+data.OcorrenciaMunicipal.municipio_id)
				.success(function(data){
					this.$.municipios = data.data;
				}.bind(this));
				// Monta combo com o programa atual
				this.$http
				.get('/api/programa?order=Programa.nome_divulgacao.asc&where=Programa.id.eq.'+data.OcorrenciaMunicipal.programa_id)
				.success(function(data){
					this.$.programas = data.data;
				}.bind(this));

			}.bind(this));
		},
		add: function(item) {
			this.related();
			this.$http
			.get('/api/grupo?order=Grupo.nome.asc&limit=100')
			.success(function(data) {
			}.bind(this));
		},
		save: function() {
			this.$http
			.post(
				'/api/ocorrencia_municipal/index',
				this.$.form.OcorrenciaMunicipal
			)
			.success(function(data) {
				this.Data.message('success', 'Usuário salvo!', { timeout: 3000 });
				location.href='#/ocorrencia';
			}.bind(this))
			.error(function() {
				this.Data.message('danger', 'Erro ao salvar!', { timeout: 3000 });
				location.href='#/ocorrencia';
			}.bind(this));
		},
		cancel: function() {
			location.href = '#/ocorrencia';
		},
		formInvalid: function() {
			if (this.$.formOcorrencia.$invalid) return true;
			else return false;
		},
		buscaMunicipios: function() {
			this.$http
			.get('/api/municipio?order=Municipio.nome.asc&where=Municipio.nome.lk.'+this.$.form.OcorrenciaMunicipal.busca_municipio)
			.success(function(data){
				this.$.municipios = data.data;
			}.bind(this));
		},
		buscaProgramas: function() {
			this.$http
			.get('/api/programa?order=Programa.nome_divulgacao.asc&where=Programa.nome_divulgacao.lk.'+this.$.form.OcorrenciaMunicipal.busca_programa)
			.success(function(data){
				this.$.programas = data.data;
			}.bind(this));
		}
	}
});
