estacaoApp.cC({
	name: 'permissaoCtrl',
	inject: ['$scope','$http','messageCenterService'],
	init: function() {
		this.$.screen = 'listing';
		this.$.listing = [];
		this.load(1);
	},
	load: function(page) {
		this.$http
		.get('/api/permissao/index/page:'+page+'?order=Permissao.nome.asc')
		.success(function(data) {
			this.$.listing = data.data;
			this.$.paginator = data.paginator;
		}.bind(this));
	},
	edit: function(item) {
		
		this.$.form = item;
		this.$.screen = 'form';
		
	},
	add: function(item) {
		
		this.$.form = {
			Permissao: {}
		};
		this.$.screen = 'form';
	},
	save: function() {
		this.$http
		.post(
			'/api/permissao/index',
			this.$.form
		)
		.success(function(data) {
			this.messageCenterService.add('success', 'Permissão salva!', { timeout: 3000 });
			this.load(this.$.paginator.Permissao.page);
			this.$.screen = 'listing';	
		}.bind(this))
		.error(function() {
			this.messageCenterService.add('danger', 'Erro ao salvar!', { timeout: 3000 });
			this.$.screen = 'listing';	
		}.bind(this));
	},
	cancel: function() {
		this.$.form = [];
		this.$.screen = 'listing';		
	},
	del: function(item) {
		this.$http
		.delete('/api/permissao/index/'+item.Permissao.id)
		.success(function() {
			this.load(this.$.paginator.Permissao.page);
			this.$.screen = 'listing';
		}.bind(this));
	}
});
