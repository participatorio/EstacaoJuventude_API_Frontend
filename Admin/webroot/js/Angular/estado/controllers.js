estacaoApp.cC({
	name: 'estadoCtrl',
	inject: ['$scope','$http','Data','$routeParams'],
	init: function() {
		this.load(1);
		this.Data.init();
	},
	methods: {
		load: function(page) {
			this.$http
			.get('/api/estado/index/page:'+page+'?order=Estado.nome.asc')
			.success(function(data) {
				this.$.listing = data.data;
				this.$.paginator = data.paginator;
			}.bind(this));
		},
		del: function(item) {
			if (window.confirm('Tem Certeza?')) {
				this.$http
				.delete('/api/estado/index/'+item.Estado.id)
				.success(function() {
					messageCenterService.add('success', 'Estado excluído com sucesso!', { timeout: 3000 })
					this.load(this.$.paginator.page);
				}.bind(this))
				.error(function() {
					messageCenterService.add('error', 'Erro ao excluir!', { timeout: 3000 })					
				}.bind(this));
			}
		},
		go: function(where, id) {
			
			switch (where) {
				case 'add':
					location.href = '#/estado/add';
					break;
				case 'edit':
					location.href = '#/estado/edit/'+id;
					break;
			}
		}
	}

});

estacaoApp.cC({
	name: 'estadoFormCtrl',
	inject: ['$scope','$http','Data','$routeParams'],
	init: function() {
		if (this.$routeParams.id) {
			this.edit();
		} else {
			this.add();
		}	
	},
	methods: {
		add: function(item) {
			
			this.$.form = {
					Estado: {
						latitude: -15.9,
						longitude: -47.89
					}
			};
			this.$.screen = 'form';
	
		},
		edit: function() {
			this.$http
			.get('/api/estado/index/'+this.$routeParams.id)
			.success(function(data){
				this.$.form = data;
			}.bind(this));
		},
		save: function() {
			this.$http
			.post(
				'/api/estado/index',
				this.$.form
			)
			.success(function(data) {
				this.Data.flashMsgs.push({
					type:'success', 
					msg: 'Salvo com sucesso!', 
					options: { timeout: 3000 }
				});
				location.href = '#/estado';
			}.bind(this))
			.error(function() {
				this.messageCenterService.add('danger', 'Erro ao salvar!', { timeout: 3000 });
				location.href = '#/estado';	
			}.bind(this));
		},
		cancel: function() {
			location.href = '#/estado';
		},
		moveMarker: function(me) {
			this.$.form.Estado.latitude = me.latLng.k;
			this.$.form.Estado.longitude = me.latLng.D;
		}
	}
});

