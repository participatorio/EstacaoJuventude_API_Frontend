estacaoApp.cC({
	name: 'menuCtrl',
	inject: ['$scope'],
	methods: {
		nothelp: function(){
			console.log('helper inside menu');
		}
	},
	init: function() {
		
		this.$.menus = [
			{
				align: 'pull-left',
				items: [
					{
						title: 'Módulos',
						permit: 'modulo.menu',
						children: [
							{
								title: 'Ocorrência',
								url: '#/ocorrencia',
								permit: 'ocorrencia.menu'
							},
							{
								title: 'Programa',
								url: '#/programa',
								permit: 'programa.menu'
							},
							{
								title: 'Temática',
								url: '#/tematica',
								permit: 'tematica.menu'
							}
						]
					},
					{
						title: 'Tabelas',
						children: [
							{
								title: 'Estado',
								url: '#/estado',
								permit: 'estado.menu'
							},
							{
								title: 'Município',
								url: '#/municipio',
								permit: 'municipio.menu'
							},
							{
								title: 'Órgão Executor',
								url: '#/orgao',
								permit: 'orgao.menu'
							}
						]
					}
				]
			},
			{
				align: 'pull-right',
				items: [
					{
						title: '',
						icon: 'fa fa-question fa-2x',
						tooltip: 'Ajuda',
						children: [],
						func: this.$.help
					}
				]
			},
			{
				align: 'pull-right',
				items: [
					{
						title: 'Segurança',
						children: [
							{
								title: 'Usuário',
								url: '#/usuario',
								permit: 'usuario.menu'
							},
							{
								title: 'Grupo',
								url: '#/grupo',
								permit: 'grupo.menu'
							}
						]
					}
				]
			},
			{
				align: 'pull-right',
				items: [
					{
						title: 'Mensagens',
						children: [
							{
								title: 'Caixa de Entrada',
								url: '#/mensagem',
								permit: 'mensagem.menu'
							}
						]
					}
				]
			}		
		];
	}
});