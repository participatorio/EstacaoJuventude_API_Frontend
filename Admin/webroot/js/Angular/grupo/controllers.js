estacaoApp.cC({
	name: 'grupoCtrl',
	inject: ['$scope','$http','messageCenterService'],
	init: function() {
		this.$.screen = 'listing';
		this.$.listing = [];
		this.load(1);
	},
	methods: {
		load: function(page) {
			this.$http
			.get('/api/grupo/index/page:'+page+'?populate=Pai&order=Grupo.nome.asc')
			.success(function(data) {
				this.$.listing = data.data;
				this.$.paginator = data.paginator;
			}.bind(this));
		},
		edit: function(item) {
			this.$http
			.get('/api/grupo?order=Grupo.nome.asc&limit=100&where=Grupo.id.ne.'+item.Grupo.id)
			.success(function(data) {
				this.$.grupos = data.data;
				this.$.form = item;
				this.$.screen = 'form';
			}.bind(this));
		},
		add: function(item) {
			this.$http
			.get('/api/grupo?order=Grupo.nome.asc&limit=100')
			.success(function(data) {
				this.$.grupos = data.data;
				this.$.form = {
					Grupo: {}
				};
				this.$.screen = 'form';
			}.bind(this));
		},
		save: function() {
			if (this.$.formGrupo.$valid) {
				this.$http
				.post(
					'/api/grupo/index',
					this.$.form
				)
				.success(function(data) {
					this.messageCenterService.add('success', 'Usuário salvo!', { timeout: 3000 });
					this.load(this.$.paginator.Grupo.page);
					this.$.screen = 'listing';	
				}.bind(this))
				.error(function() {
					this.messageCenterService.add('danger', 'Erro ao salvar!', { timeout: 3000 });
					this.$.screen = 'listing';	
				}.bind(this));
			} else {
				this.messageCenterService.add('danger','Formulário inválido');
			}
		},
		cancel: function() {
			this.$.form = [];
			this.$.screen = 'listing';		
		},
		del: function(item) {
			this.$http
			.delete('/api/usuario/index/'+item.Grupo.id)
			.success(function() {
				this.load(this.$.paginator.Grupo.page);
				this.$.screen = 'listing';
			}.bind(this));
	}
	}
});
