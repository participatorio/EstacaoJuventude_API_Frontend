estacaoApp.service('Data',['messageCenterService', function( messageCenterService ) {

	var service = this;
	
	this.title = 'Estação Juventude';
	// Info de Login de Usuario 
	this.user = {};
	
	// Mensagens
	this.flashMsgs = [];
	
	// Sistema de Segurança
	this.security = {
		isAdmin: false,
		permits: {}
	}
	
	this.init = function() {
	
		if (service.flashMsgs.length > 0) {
			for (i in service.flashMsgs) {
				messageCenterService.add( 
					service.flashMsgs[i].type, 
					service.flashMsgs[i].msg, 
					service.flashMsgs[i].options 
				);
			}
			service.flashMsgs = [];
		}
		
	}
	
	this.message = function(type, msg, options) {
		this.flashMsgs.push(type, msg, options);
	}
	
	
	
}]);
