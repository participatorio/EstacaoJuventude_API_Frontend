<?php
	class Estado extends AppModel {
		
		public $useTable = 'estados';
		
		public $hasMany = array(
		
			'Municipio' => array(
				'className' => 'Api.Municipio',
				'foreignKey' => 'estado_id'
			)
		
		);
		
	}