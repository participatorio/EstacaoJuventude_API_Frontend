<?php
	class Municipio extends AppModel {
		
		public $useTable = 'municipios';
		
		public $belongsTo = array(
		
			'Estado' => array(
				'className' => 'Api.Estado',
				'foreignKey' => 'estado_id'
			)
		
		);
		
	}