<?php
	class Menu extends AppModel {
		
		public $useTable = 'menus';
		
		public $belongsTo = array(
		
			'Permissao' => array(
				'className' => 'Api.Permissao',
				'foreignKey' => 'permissao_id'
			)
		
		);
		
	}