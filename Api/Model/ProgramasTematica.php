<?php
	class ProgramasTematica extends AppModel {
		
		public $belongsTo = array(
		
			'Tematica' => array(
				'foreignKey' => 'tematica_id',
				'className' => 'Api.Tematica'
			),
			
			'Programa' => array(
				'foreignKey' => 'programa_id',
				'className' => 'Api.Programa'
			)
		
		);
		
	}