<?php
	class GrupoPermissao extends AppModel {
		
		public $useTable = 'grupos_permissoes';
		
		public $belongsTo = array(
		
			'Grupo' => array(
				'className' => 'Api.Grupo',
				'foreignKey' => 'grupo_id'
			),
			
			'Permissao' => array(
				'className' => 'Api.Permissao',
				'foreignKey' => 'permissao_id'
			)
		
		);
		
	}