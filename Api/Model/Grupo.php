<?php
	class Grupo extends AppModel {
		
		public $useTable = 'grupos';
		
		public $hasMany = array(
			'GrupoPermissao' => array(
				'className' => 'Api.GrupoPermissao',
				'foreignKey' => 'grupo_id'
			)
		);
		
		public $belongsTo = array(
			'Pai' => array(
				'className' => 'Api.Grupo',
				'foreignKey' => 'parent_id'
			)
		);
		
	}