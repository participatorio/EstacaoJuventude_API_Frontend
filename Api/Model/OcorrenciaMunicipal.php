<?php
class OcorrenciaMunicipal extends AppModel {

	public $useTable = 'ocorrencias_municipais';
	
	public $belongsTo = array(
		'Municipio' => array(
			'className' => 'Api.Municipio',
			'foreignKey' => 'municipio_id'
		), 
		'Programa' => array(
			'className' => 'Api.Programa',
			'foreignKey' => 'programa_id'
		),
		'Status' => array(
			'className' => 'Api.Status',
			'foreignKey' => 'status_id'
		),
		'Situacao' => array(
			'className' => 'Api.Situacao',
			'foreignKey' => 'situacao_id'
		)
	);
	
}
