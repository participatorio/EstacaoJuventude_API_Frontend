<?php
	class Tematica extends AppModel {
		
		public $hasMany = array(
			'ProgramasTematica' => array(
				'foreignKey' => 'tematica_id',
				'className' => 'Api.ProgramasTematica'
			),
			'Filhos' => array(
				'className' => 'Api.Tematica',
				'foreignKey' => 'parent_id'
			)
		);
		
		public $belongsTo = array(
			'Pai' => array(
				'className' => 'Api.Tematica',
				'foreignKey' => 'parent_id'
			)
		);
		
	}