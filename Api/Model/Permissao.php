<?php
	class Permissao extends AppModel {
		
		public $useTable = 'permissoes';
		
		public $hasMany = array(
			'GrupoPermissao' => array(
				'className' => 'Api.GrupoPermissao',
				'foreignKey' => 'permissao_id'
			)
		);
		
	}