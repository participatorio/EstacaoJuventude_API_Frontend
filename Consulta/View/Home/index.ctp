<div style="position:absolute; top: 10px; right: 10px; z-index:1000; min-width: 300px;" mc-messages></div>

<section class="container-fluid">
	
	<nav class="navbar navbar-default navbar-fixed-top" ng-controller="menuCtrl">
		<div class="container-fluid">
			<div class="navbar-header">
				<a class="navbar-brand" href="#/">
					Estação Juventude
				</a>
			</div>
			
			<div ng-cloak class="ng-cloak" class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				
				<ul ng-repeat="menu in menus" class="nav navbar-nav {{menu.align}}">
					<li ng-if="item.children.length == 0" ng-repeat="item in menu.items"><a href="#{{item.url}}">{{item.title}}</a></li>
					<li ng-if="item.children.length > 0" ng-repeat="item in menu.items" class="dropdown">
						<a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{{item.title}} <span class="caret"></span></a>
						<ul class="dropdown-menu" role="menu">
							<li ng-repeat="subitem in item.children"><a href="#{{subitem.url}}">{{subitem.title}}</a></li>
						</ul>
					</li>
				</ul>
				
			</div>

		</div>
	</nav>
	
	<div class="row">
		<div class="col-md-12">
			<div ng-view></div>
		</div>
	</div>
</section>
