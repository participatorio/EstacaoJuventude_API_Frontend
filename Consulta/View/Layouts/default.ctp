<!DOCTYPE html>
<html ng-app="estacaoApp" ng-controller="mainController">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<title>Estação Juventude</title>

		<!-- CSS -->
		<link rel="stylesheet" href="/consulta/bower_components/angular/angular-csp.css">
		<!-- Twitter Bootstrap -->
		<link rel="stylesheet" href="/consulta/bower_components/bootstrap/dist/css/bootstrap.css">
		<!-- Theme -->
		<link rel="stylesheet" href="/consulta/bower_components/bootswatch/lumen/bootstrap.css">
		<!-- Font Awesome -->
		<link rel="stylesheet" href="/consulta/bower_components/font-awesome/css/font-awesome.min.css">
				<!-- App CSS -->
		<link rel="stylesheet" href="/consulta/css/style.css">
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 10]>
			<script src="/consulta/bower_components/html5shiv/dist/html5shiv.min.js"></script>
			<script src="/consulta/bower_components/respond/dest/respond.min.js"></script>
		<![endif]-->
		<!-- Scripts -->
		<script src="//maps.googleapis.com/maps/api/js"></script>
		<script src="/consulta/bower_components/jquery/dist/jquery.min.js"></script>
		<!-- Twitter Bootstrap -->
		<script src="/consulta/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
		<!-- Angular JS -->
		<script src="/consulta/bower_components/angular/angular.min.js"></script>
		<!-- Angular Modules -->
		<!-- TextAngular -->
		<script src="/consulta/bower_components/textAngular/dist/textAngular-rangy.min.js"></script>
		<script src="/consulta/bower_components/textAngular/dist/textAngular-sanitize.min.js"></script>
		<script src="/consulta/bower_components/textAngular/dist/textAngular.min.js"></script>
		<!-- Angular UI Bootstrap -->
		<script src="/consulta/bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js"></script>
		<!-- ng-Maps -->
		<script src="/consulta/bower_components/ngmap/build/scripts/ng-map.min.js"></script>
		<!-- Route -->
		<script src="/consulta/bower_components/angular-route/angular-route.min.js"></script>
		<!-- Classy -->
		<script src="/consulta/bower_components/angular-classy/angular-classy.min.js"></script>
		<!-- Message Center -->
		<script src="/consulta/bower_components/message-center/message-center.js"></script>
		<!-- Angular App -->
		<script src="/consulta/js/Angular/app.js"></script>
		<!-- Angular ngRoutes -->
		<script src="/consulta/js/Angular/routes.js"></script>
		<!-- Angular Services -->
		<script src="/consulta/js/services/data.js"></script>
		<!-- Angular Controllers -->
		<script src="/consulta/js/Angular/mainController.js"></script>
		<script src="/consulta/js/Angular/menu/controllers.js"></script>
		<script src="/consulta/js/Angular/home/controllers.js"></script>
	</head>
	<body>
		<div class="content">
		<?php echo $this->fetch('content'); ?>
		</div>
	</body>
</html>
