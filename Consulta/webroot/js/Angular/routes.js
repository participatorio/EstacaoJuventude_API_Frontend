estacaoApp.config(
	['$routeProvider',
	function($routeProvider) {
	$routeProvider
	.when('/', {
		templateUrl: '/consulta/js/Angular/home/index.html',
		controller: 'mainController'
	})	
	// Erros
	.otherwise({
		templateUrl: '/admin/js/Angular/errors/404.html'
	});
}]);