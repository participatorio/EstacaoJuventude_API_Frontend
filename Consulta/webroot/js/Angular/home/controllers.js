estacaoApp.cC({
	name: 'homeCtrl',
	inject: ['$scope','$http','Data'],
	init: function() {
		// Mapa
		this.$.map = {
			center: {
				latitude: -15, 
				longitude: -47
			},
			zoom: 4
		};
		this.$.markers ={};
		this.$.theme = 'lumen';
		
		this.load();
	},
	watch: {
		'{object}theme': '_changeTheme'
  	},
  	methods: {
	  	load: function() {
		  	var url = '/api/ocorrencia_municipal/index/page:1?populate=Municipio.Estado,Programa,Status,Situacao&order=OcorrenciaMunicipal.inicio_inscricoes.desc';
	  	},
  	  	_changeTheme: function() {
			this.Data.setTheme(this.$.theme);
			console.log(this.$.theme);
	  	},
		loadEstados: function() {
			this.$http
			.get('/api/estado?limit=30')
			.success(function(data){
				this.$.estados = data;
				this.$.markers.estados = this._buildMarkers(data.data);
			}.bind(this));
		},
		_buildMarkers: function(data) {
			var markers = [];
			for(item in data) {
				var sigla = data[item].Estado.sigla.toLowerCase();
				
				markers.push({
					id: data[item].Estado.id,
					sigla: data[item].Estado.sigla,
					icon: '/consulta/img/bandeiras/icons/bandeira-'+data[item].Estado.sigla.toLowerCase()+'.png',
					latitude: data[item].Estado.latitude, 
					longitude: data[item].Estado.longitude
				});
			}
			return markers;
		}
	}
});