estacaoApp.classy.controller({
	name: 'headerCtrl',
	inject: ['$scope','Data'],
	init: function() {
		//this.$.title = this.Data.title;
		this.$.cssTheme = 'lumen';
	},
	changeTitle: function(title) {
		this.$.title = title;
	},
	getTheme: function() {
		return this.Data.theme;
	}
});

